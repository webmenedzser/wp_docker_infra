# Expires map
map $sent_http_content_type $expires {
    default                    off;
    text/html                  epoch;
    text/css                   max;
    application/javascript     max;
    ~image/                    max;
}

server {
    listen 80;
    server_name example.com;

    location / {
        return 301 https://www.example.com$request_uri;
    }
}

server {
    listen 80;
    listen 443;
    server_name www.example.com;

    root /var/www/html/example.com;
    index index.php;

    access_log /var/log/nginx/example-access.log;
    error_log /var/log/nginx/example-error.log;

    include conf.d/d.cloudflare;
    include conf.d/d.drop;

    sendfile on;
    sendfile_max_chunk 512k;
    tcp_nopush  on;
    tcp_nodelay on;
    server_tokens off;
    server_name_in_redirect off;

    keepalive_timeout  5;
    keepalive_requests 500;
    lingering_time 20s;
    lingering_timeout 5s;
    keepalive_disable msie6;

    gzip on;
    gzip_vary   on;
    gzip_disable "MSIE [1-6]\.";
    gzip_static on;
    gzip_min_length   1400;
    gzip_buffers      32 8k;
    gzip_http_version 1.0;
    gzip_comp_level 5;
    gzip_proxied    any;
    gzip_types text/plain text/css text/xml application/javascript application/x-javascript application/xml application/xml+rss application/ecmascript application/json image/svg+xml;

    client_body_buffer_size 256k;
    client_body_in_file_only off;
    client_body_timeout 15s;
    client_header_buffer_size 64k;
    ## how long a connection has to complete sending
    ## it's headers for request to be processed
    client_header_timeout  8s;
    client_max_body_size 128m;
    connection_pool_size  512;
    directio  4m;
    ignore_invalid_headers on;
    large_client_header_buffers 8 64k;
    output_buffers   8 256k;
    postpone_output  1460;
    proxy_temp_path  /tmp/nginx_proxy/;
    request_pool_size  32k;
    reset_timedout_connection on;
    send_timeout     15s;
    types_hash_max_size 2048;

    # Process timeout


    # for nginx proxy backends to prevent redirects to backend port
    # port_in_redirect off;

    open_file_cache max=50000 inactive=90s;
    open_file_cache_valid 120s;
    open_file_cache_min_uses 2;
    open_file_cache_errors off;
    open_log_file_cache max=10000 inactive=30s min_uses=2;

    expires $expires;

    location ~* \.(css|js|png|jpg|jpeg|gif|ico)$ {
        expires 7d;
        log_not_found off;
    }

    location ~ /(wp-config.php|readme.html|licence.txt) {
        return 404;
    }

    if ($request_method !~ ^(GET|POST|HEAD)$ ) {
        return 444;
    }

    set $virtualdir "";
    set $realdir "";

    if ($request_uri ~ ^/([^/]*)/.*$ ) {
        set $virtualdir /$1;
    }

    if (-d "$document_root$virtualdir") {
        set $realdir "${virtualdir}";
    }

    location / {
        index  index.html index.htm index.php;
        try_files $uri $uri/ $realdir/index.php?$args;
    }

    location ~ \.php$ {
        fastcgi_buffers 8 256k;
        fastcgi_buffer_size 128k;
        fastcgi_intercept_errors on;
        fastcgi_read_timeout 600;
        try_files $uri =404;
        fastcgi_split_path_info ^(.+\.php)(/.+)$;
        fastcgi_pass php-fpm:9000;
        fastcgi_index index.php;
        include fastcgi_params;
        fastcgi_param SCRIPT_FILENAME $document_root$fastcgi_script_name;
        fastcgi_param PATH_INFO $fastcgi_path_info;
    }
}

server {
    listen 80;
    listen 443;
    server_name staging.example.com;

    root /var/www/html/staging.example.com;
    index index.php;

    access_log /var/log/nginx/staging-example-access.log;
    error_log /var/log/nginx/staging-example-error.log;

    include conf.d/d.cloudflare;
    include conf.d/d.drop;

    sendfile on;
    sendfile_max_chunk 512k;
    tcp_nopush  on;
    tcp_nodelay on;
    server_tokens off;
    server_name_in_redirect off;

    keepalive_timeout  5;
    keepalive_requests 500;
    lingering_time 20s;
    lingering_timeout 5s;
    keepalive_disable msie6;

    gzip on;
    gzip_vary   on;
    gzip_disable "MSIE [1-6]\.";
    gzip_static on;
    gzip_min_length   1400;
    gzip_buffers      32 8k;
    gzip_http_version 1.0;
    gzip_comp_level 5;
    gzip_proxied    any;
    gzip_types text/plain text/css text/xml application/javascript application/x-javascript application/xml application/xml+rss application/ecmascript application/json image/svg+xml;

    client_body_buffer_size 256k;
    client_body_in_file_only off;
    client_body_timeout 15s;
    client_header_buffer_size 64k;
    ## how long a connection has to complete sending
    ## it's headers for request to be processed
    client_header_timeout  8s;
    client_max_body_size 128m;
    connection_pool_size  512;
    directio  4m;
    ignore_invalid_headers on;
    large_client_header_buffers 8 64k;
    output_buffers   8 256k;
    postpone_output  1460;
    proxy_temp_path  /tmp/nginx_proxy/;
    request_pool_size  32k;
    reset_timedout_connection on;
    send_timeout     15s;
    types_hash_max_size 2048;

    # Process timeout


    # for nginx proxy backends to prevent redirects to backend port
    # port_in_redirect off;

    open_file_cache max=50000 inactive=90s;
    open_file_cache_valid 120s;
    open_file_cache_min_uses 2;
    open_file_cache_errors off;
    open_log_file_cache max=10000 inactive=30s min_uses=2;

    expires $expires;

    location ~* \.(css|js|png|jpg|jpeg|gif|ico)$ {
        expires 7d;
        log_not_found off;
    }

    location ~ /(wp-config.php|readme.html|licence.txt) {
        return 404;
    }

    if ($request_method !~ ^(GET|POST|HEAD)$ ) {
        return 444;
    }

    set $virtualdir "";
    set $realdir "";

    if ($request_uri ~ ^/([^/]*)/.*$ ) {
        set $virtualdir /$1;
    }

    if (-d "$document_root$virtualdir") {
        set $realdir "${virtualdir}";
    }

    location / {
        index  index.html index.htm index.php;
        try_files $uri $uri/ $realdir/index.php?$args;
    }

    location ~ \.php$ {
        fastcgi_buffers 8 256k;
        fastcgi_buffer_size 128k;
        fastcgi_intercept_errors on;
        fastcgi_read_timeout 600;
        try_files $uri =404;
        fastcgi_split_path_info ^(.+\.php)(/.+)$;
        fastcgi_pass php-fpm:9000;
        fastcgi_index index.php;
        include fastcgi_params;
        fastcgi_param SCRIPT_FILENAME $document_root$fastcgi_script_name;
        fastcgi_param PATH_INFO $fastcgi_path_info;
    }
}
